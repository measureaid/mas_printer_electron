﻿
(function () {
	'use strict';

	var fs = require('fs');
	var exec = require('child_process').exec;

	var json2csv = require('json2csv');
	var intformat = require('biguint-format'),
		FlakeId = require('flake-idgen');
	var flakeIdGen1 = new FlakeId();

	var _ = require('lodash');
	var path = require('path');

	// src/main.js
	var electron = require('electron');
	var app = electron.app;
	var BrowserWindow = electron.BrowserWindow;
	var ipcMain = electron.ipcMain;
	var debug = /--debug/.test(process.argv[2]);


	// Electron 개발자에게 crash-report를 보냄.
	// require('crash-reporter').start();

	// 윈도우 객체를 전역에 유지합니다. 만약 이렇게 하지 않으면
	// 자바스크립트 GC가 일어날 때 창이 자동으로 닫혀버립니다.
	var mainWindow = null;

	// 환경설정 관련
	var config_file_path = path.join(__dirname, 'config.json');
	var config_file = fs.readFileSync(config_file_path);

	var config = JSON.parse(config_file);

	var TEMPLATE_PATH, BARTENDER_PATH, SERVER_URL, CARD_TEMPLATE_PATH, CONTAINER_TEMPLATE_PATH;

	var print;
	var printContainer;
	var printCard;
	var now_printing = false;


	function makeSingleInstance() {
		return app.makeSingleInstance(function () {
			if(mainWindow) {
				if(mainWindow.isMinimized()) mainWindow.restore();
				mainWindow.focus();
			}
		});
	}

	function initialize() {
		var shouldQuit = makeSingleInstance();
		if(shouldQuit) return app.quit();

		if(process.platform === 'win32') {
			config.TEMPLATE_PATH = path.join(__dirname, 'templates', 'barcode.btw');
			config.CONTAINER_TEMPLATE_PATH = path.join(__dirname, 'templates', 'container.btw');
			config.CARD_TEMPLATE_PATH = path.join(__dirname, 'templates', 'card.btw');

			TEMPLATE_PATH = config.TEMPLATE_PATH;
			BARTENDER_PATH = config.BARTENDER_PATH;
			CARD_TEMPLATE_PATH = config.CARD_TEMPLATE_PATH;
			CONTAINER_TEMPLATE_PATH = config.CONTAINER_TEMPLATE_PATH;

			console.log(config);
			fs.writeFileSync(config_file_path, JSON.stringify(config));
		}

		function createWindow() {
			var windowOptions = {
				width: 800,
				// minWidth: 680,
				height: 600,
				show: true,
				// debug:true,
				// 'fullscreen': true,
				// 'frame': false,
				// 'kiosk': true,
			};
			if(process.platform === 'linux') {
				windowOptions.icon = path.join(__dirname, '/assets/app-icon/png/512.png');
			}

			mainWindow = new BrowserWindow(windowOptions);
			mainWindow.loadURL(path.join('file://', __dirname, '/app', 'index.html'));
			// Launch fullscreen with DevTools open, usage: npm run debug
			if(debug) {
				mainWindow.webContents.openDevTools();
				mainWindow.maximize();
			}

			mainWindow.on('closed', function () {
				mainWindow = null;
			});
		}

		app.on('ready', function () {
			createWindow();
		});

		app.on('window-all-closed', function () {
			if(process.platform !== 'darwin') {
				app.quit();
			}
		});

		app.on('activate', function () {
			if(mainWindow === null) {
				createWindow();
			}
		});
	}

	initialize();

		ipcMain.on('templateUrl', function (event, arg) {
			// console.log(arg); // prints "ping"
			event.returnValue = templatePath;
		});

		ipcMain.on('print', function (event, arg) {
			print(arg, function (err, result) {
				event.sender.send('print-result', {
					type: 'normal',
					err: err,
					result: result
				});
			});
		});

		ipcMain.on('printContainer', function (event, arg) {
			printContainer(arg, function (err, result) {
				event.sender.send('print-result', {
					type: 'container',
					err: err,
					result: result
				});
			});
		});

		ipcMain.on('printCard', function (event, arg) {
			printCard(arg, function (err, result) {
				event.sender.send('print-result', {
					type: 'card',
					err: err,
					result: result
				});
			});
		});


		ipcMain.on('editTemplate', function (event, arg, type) {
			var template_path = config.TEMPLATE_PATH;

			if(type==='card'){
				template_path = config.CARD_TEMPLATE_PATH;
			}
			if(type==='container'){
				template_path = config.CONTAINER_TEMPLATE_PATH;
			}

			var command = config.BARTENDER_PATH + ' ' + template_path;
			console.log(command);
			exec(command, function (error, stdout, stderr) {});
		});


		//서버 접속 관련
		var socketIOClient = require('socket.io-client');
		var sailsIOClient = require('sails.io.js');
		var io = sailsIOClient(socketIOClient);
		// //
		io.sails.url = config.SERVER_URL;
		io.sails.transports = ['websocket'];
		// //
		var isConnected = false;
		//
		// //
		io.socket.on('connect', function () {
			console.log('connected');
			registerPrinter();
		});
		// //
		io.socket.on('disconnect', function () {
			isConnected = false;
		});
		io.socket.on('print', function (data) {
			console.log('start print');
			print(data, function () {
				console.log(arguments);
			});
		});

		io.socket.on('printContainer', function (data) {
			console.log('start print');
			printContainer(data, function () {
				console.log(arguments);
			});
		});

		io.socket.on('printCard', function (data) {
			console.log('start print');
			printCard(data, function () {
				console.log(arguments);
			});
		});
		//
		// //
		//

		var registerPrinter = function () {
			io.socket.post('/printer', function serverResponded(body, JWR) {
				console.log(body, '???', JWR);
				console.log('registered.');
				isConnected = true;
				// console.log('Sails responded with: ', body);
				// console.log('with headers: ', JWR.headers);
				// console.log('and with status code: ', JWR.statusCode);
			});
		};

		var print;
		var printContainer;
		var printCard;
		var now_printing = false;
		if (process.platform === 'win32') {
			print = function print (data, callback) {
				//make csv
				var fs = require('fs');
				var json2csv = require('json2csv');
				var fields = ['name', 'code', 'container_name', 'quantity', 'unit', 'bundle_unit','bundle_quantity','user_name', 'createdAt'];

				if (!_.isArray(data)) {
					return;
				}

				var duplicatedData = [];

				_.map(data, function (datum) {
					if (datum.hasOwnProperty('print_quantity') && datum.print_quantity) {
						for (var j = 0; j < datum.print_quantity; j++) {
							duplicatedData.push(datum);
						}
					} else {
						duplicatedData.push(datum);
					}
				});
				console.log('duplicatedData',duplicatedData);

				// console.log(data);
				json2csv({
					data: duplicatedData,
					fields: fields,
					quotes: ""
				}, function (err, csv) {
					if (err) {
						console.log(err);
					}
					// var filename = new Date().getTime() + '.csv';
					var filename = intformat(flakeIdGen1.next(), 'dec') + '.csv';

					fs.writeFile(path.join(__dirname, 'appData', 'temp', filename), csv, function (err) {
						if (err) {
							console.log('FILE WRITE ERROR', err, filename);
						}
						var command = config.BARTENDER_PATH + ' ' + config.TEMPLATE_PATH + ' /D=' + path.join(__dirname, 'appData', 'temp', filename) + " /P /X";
						console.log(command);
						console.log('printing...' + filename);
						// exec(command, function (error, stdout, stderr) {
						// 	callback(error, stdout);
						// });
						var times = 0;
							var now_printing_timer = setInterval(function (_command) {
								times++;
								if(now_printing == false) {
									now_printing = true;
									console.log('printing...' , duplicatedData[0].code);
									exec(_command, function (error, stdout, stderr) {
										now_printing = false;
										clearInterval(now_printing_timer);
										callback(error, stdout);
									});
								} else {
									// console.log('waiting', times);
								}
							}, 100, command);

					});
				});

			};

			printContainer = function printContainer (data, callback) {
				//make csv
				var fs = require('fs');
				var json2csv = require('json2csv');
				var fields = ['name', 'code', 'createdAt'];

				if (!_.isArray(data)) {
					return;
				}

				var duplicatedData = [];

				_.map(data, function (datum) {
					if (datum.hasOwnProperty('print_quantity') && datum.print_quantity) {
						for (var j = 0; j < datum.print_quantity; j++) {
							duplicatedData.push(datum);
						}
					} else {
						duplicatedData.push(datum);
					}
				});

				// console.log(data);
				json2csv({
					data: duplicatedData,
					fields: fields,
					quotes: ""
				}, function (err, csv) {
					if (err) {
						console.log(err);
					}
					// var filename = new Date().getTime() + '_container.csv';
					var filename = intformat(flakeIdGen1.next(), 'dec') + '.csv';

					fs.writeFile(path.join(__dirname, 'appData', 'temp', filename), csv, function (err) {
						if (err) {
							console.log('FILE WRITE ERROR', err, filename);
						}
						var command = config.BARTENDER_PATH + ' ' + config.CONTAINER_TEMPLATE_PATH + ' /D=' + path.join(__dirname, 'appData', 'temp', filename) + " /P /X";
						console.log(command);
						console.log('printing...' + filename);
						exec(command, function (error, stdout, stderr) {
							callback(error, stdout);
						});
					});
				});

			};

			printCard = function printCard (data, callback) {
				//make csv
				var fs = require('fs');
				var json2csv = require('json2csv');
				var fields = ['name', 'code', 'createdAt'];

				if (!_.isArray(data)) {
					return;
				}

				var duplicatedData = [];

				_.map(data, function (datum) {
					if (datum.hasOwnProperty('print_quantity') && datum.print_quantity) {
						for (var j = 0; j < datum.print_quantity; j++) {
							duplicatedData.push(datum);
						}
					} else {
						duplicatedData.push(datum);
					}
				});

				// console.log(data);
				json2csv({
					data: duplicatedData,
					fields: fields,
					quotes: ""
				}, function (err, csv) {
					if (err) {
						console.log(err);
					}
					// var filename = new Date().getTime() + '_card.csv';
					var filename = intformat(flakeIdGen1.next(), 'dec') + '.csv';

					fs.writeFile(path.join(__dirname, 'appData', 'temp', filename), csv, function (err) {
						if (err) {
							console.log('FILE WRITE ERROR', err, filename);
						}
						var command = config.BARTENDER_PATH + ' ' + config.CARD_TEMPLATE_PATH + ' /D=' + path.join(__dirname, 'appData', 'temp', filename) + " /P /X";
						console.log(command);
						console.log('printing...' + filename);
						exec(command, function (error, stdout, stderr) {
							callback(error, stdout);
						});
					});
				});

			};
		} else {

			print = function print() {
				console.log('인쇄', arguments);
			};

			printCard = function printCard() {
				console.log('인쇄 카드', arguments);
			};

			printContainer = function printContainer() {
				console.log('인쇄 컨테이너', arguments);
			};
		}

})();
