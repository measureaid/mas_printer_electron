(function () {
    'use strict';

    angular
        .module('mas.printer')
        .controller('PrinterController', PrinterController);

    PrinterController.$inject = ['Printer'];

    /* @ngInject */
    function PrinterController(Printer) {
        var vm = this;
        vm.submit = function (label) {
            Printer.print(label);
        };
    }
})();
