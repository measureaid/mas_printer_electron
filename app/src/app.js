(function () {
    'use strict';
    angular
        .module('masPrinterApp', [
            'ngMaterial',
            'mas.printer',
            'ui.router',
            'users'
        ])
        // .run(function ($rootScope) {
        //     var ipcRenderer = require('electron').ipcRenderer;
        //     $rootScope.templateURL = ipcRenderer.sendSync('templateUrl');
        //     console.log($rootScope.templateURL);
        //     // console.log(ipcRenderer.sendSync('synchronous-message', 'ping')); // prints "pong"
        //     //
        //     ipcRenderer.on('asynchronous-reply', function (event, arg) {
        //         console.log(arg); // prints "pong"
        //     });
        //     ipcRenderer.send('asynchronous-message', 'ping');
        //     //
        // })
        .config(function ($mdThemingProvider, $mdIconProvider) {

            $mdIconProvider
                .defaultIconSet("./assets/svg/avatars.svg", 128)
                .icon("menu", "./assets/svg/menu.svg", 24)
                .icon("share", "./assets/svg/share.svg", 24)
                .icon("google_plus", "./assets/svg/google_plus.svg", 512)
                .icon("hangouts", "./assets/svg/hangouts.svg", 512)
                .icon("twitter", "./assets/svg/twitter.svg", 512)
                .icon("phone", "./assets/svg/phone.svg", 512);

            $mdThemingProvider
                .theme('default')
                .primaryPalette('blue')
                .accentPalette('red');
        })
        .config(function ($stateProvider, $urlRouterProvider) {
            //
            // For any unmatched url, redirect to /state1
            $urlRouterProvider.otherwise("/");
            //
            // Now set up the states
            $stateProvider
                .state('dashboard', {
                    url: "/",
                    templateUrl: "src/dashboard/dashboard.html",
                    controller: 'DashboardController',
                    controllerAs: 'vm'
                })
                .state('config', {
                    url: "/config/",
                    templateUrl: "src/config/config.html",
                    controller: function ($scope) {
                        $scope.items = ["A", "List", "Of", "Items"];
                    }
                })
                .state('printer', {
                    url: "/printer/",
                    templateUrl: "src/printer/printer.html",
                    controller: 'PrinterController',
                    controllerAs: 'vm'
                });
            // .state('state2.list', {
            //     url: "/list",
            //     templateUrl: "partials/state2.list.html",
            //     controller: function ($scope) {
            //         $scope.things = ["A", "Set", "Of", "Things"];
            //     }
            // });
        });
})();
