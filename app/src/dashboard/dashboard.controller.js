(function () {
    'use strict';

    angular
        .module('mas.printer')
        .controller('DashboardController', DashboardController);

    function DashboardController() {
        var vm = this;
        vm.printTest = printTest;
        vm.editTemplate = editTemplate;

        var fields = ['name', 'code', 'container_name', 'quantity', 'unit', 'bundle_unit','bundle_quantity','user_name', 'createdAt'];
        var num = 1;
        function printTest() {
            var ipcRenderer = require('electron').ipcRenderer;
            var data = [{
                type_name: '재료이름길어진거 (허허,이것좀보게)',
                name: '재료' + num,
                code: 'B' + num + 'L1603152701',
                container_name: '숙대',
                quantity: 200,
                unit: 'EA',
                bundle_quantity: 1,
                bundle_unit: '파레트',
                user_name: '김담당',
                createdAt: new Date()
            }];
            ipcRenderer.send('print', data);
            num++;
            // console.log($rootScope.templateURL);
            // console.log(ipcRenderer.sendSync('synchronous-message', 'ping')); // prints "pong"
            //
            ipcRenderer.on('print-result', function (event, arg) {
                console.log(arg); // prints "pong"
            });
            // ipcRenderer.send('asynchronous-message', 'ping');
        }

        function editTemplate() {
          console.log('템플릿 편집');
            var ipcRenderer = require('electron').ipcRenderer;
            ipcRenderer.send('editTemplate');
        }
        activate();

        function activate() {

        }
    }
})();
